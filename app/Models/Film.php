<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    public $table = 'film';
    protected $primaryKey = 'film_id'; // default PK is 'id'
}
