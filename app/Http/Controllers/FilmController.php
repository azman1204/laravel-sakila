<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Film;
use  PhpOffice\PhpWord\TemplateProcessor;

class FilmController extends Controller {
    // carian film
    public function search(Request $req) {
        if($req->isMethod('post')) {
            // on submit search button
            $title = $req->title;
            $rental_rate_from = $req->rental_rate_from;
            $rental_rate_to = $req->rental_rate_to;
            $q = Film::where('title', 'like', "%$title%");
            
            if (! empty($rental_rate_from) && ! empty($rental_rate_to)) {
                $q->whereBetween('rental_rate', [$rental_rate_from, $rental_rate_to]);
            }

            $films = $q->paginate(20);
        } else {
            // on klik / type on URL
            $films = Film::paginate(20);
        }
        
        $no = $films->firstItem(); // return current first item pd sesuatu page
        return view('film.search')->with('films', $films)->with('no', $no);
    }

    // generate ms word from a template
    public function word() {
        $path = storage_path() . '/app/';
        $file = $path . 'film.docx';
        $processor = new TemplateProcessor($file);

        $film = Film::find(1);
        $processor->setValue('title', $film->title);
        $processor->setValue('description', $film->description);
        $processor->setValue('rental_rate', $film->rental_rate);
        $processor->saveAs($path.'film2.docx');
    }
}
