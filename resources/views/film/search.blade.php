@extends('layouts.master')
@section('title', 'Film Search')
@section('content')
<!-- search form -->
<div class="card">
    <div class="card-body">
        <form method="POST" action="/film-search">
            @csrf
            <div class="row">
                <div class="col-md-2">Title</div>
                <div class="col-md-4"><input type="text" class="form-control" name="title"></div>
                <div class="col-md-2">Rental Rate From</div>
                <div class="col-md-4"><input type="text" class="form-control" name="rental_rate_from"></div>
            </div>
            <div class="row mt-2">
                <div class="col-md-2">Rental Rate To</div>
                <div class="col-md-4"><input type="text" class="form-control" name="rental_rate_to"></div>
                <div class="col-md-2"><input type="submit" class="btn btn-primary" value="Search"></div>
            </div>
        </form>
    </div>
</div>

<!-- search result -->
<i>{{ $films->total() }} films found. Display {{ $films->firstItem() }} to {{ $films->lastItem() }}</i>
<table class="table table-bordered table-striped">
    <tr>
        <th>#</th>
        <th>Title</th>
        <th>Description</th>
        <th>Release Year</th>
        <th>Rental Rate</th>
    </tr>
    @foreach($films as $film)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ $film->title }}</td>
            <td>{{ $film->description }}</td>
            <td>{{ $film->release_year }}</td>
            <td>{{ $film->rental_rate }}</td>
        </tr>
    @endforeach
</table>

{{ $films->links() }}
@endsection