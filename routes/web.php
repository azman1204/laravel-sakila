<?php

use App\Http\Controllers\FilmController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
// any = support GET or POST
Route::any('/film-search', [FilmController::class, 'search']);
Route::get('/film-word', [FilmController::class, 'word']);